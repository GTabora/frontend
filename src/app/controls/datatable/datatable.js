import React from "react";
import {
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  getHandlerTableChange
} from "../../../_metronic/_helpers";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { Pagination } from "../../../_metronic/_partials/controls";
import BootstrapTable from "react-bootstrap-table-next";

const CustomDatatable = ({
  entitie,
  paginationProps,
  setPaginationProps,
  columns,
}) => {
 
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationProps)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={false} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entitie === null ? [] : entitie}
                columns={columns}
                onTableChange={getHandlerTableChange(setPaginationProps)}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entitie || entitie.data} />
                <NoRecordsFoundMessage entities={entitie || entitie.data} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
};

export default CustomDatatable;
