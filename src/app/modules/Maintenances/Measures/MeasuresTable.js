import React, { useEffect, useState } from "react";
import { useIntl } from "react-intl";
import { sortCaret } from "../../../../_metronic/_helpers";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { axiosAPI } from "../../../../API.js";
import { MeasureEdit } from "./MeasureEdit";
import SnackbarAlert from "../../../controls/snackbar/snackbar";
import CustomDatatable from "../../../controls/datatable/datatable";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { MeasureDeleteDialog } from "./MeasureDeleteDialog";

export function MeasuresTable({ isNewMeasure, setIsNewMeasure }) {
  const intl = useIntl();
  const [entitie, setEntitie] = useState([]);
  const [pagination, setPagination] = useState({
    totalSize: 10,
    sizePerPage: 3,
    page: 1,
    custom: true
    // sizePerPageList: uiHelpers.sizePerPageList
  });
  const [selectedMeasure, setSelectedMeasure] = useState({});
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [showEditDialog, setShowEditDialog] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showSnackbar, setShowSnackbar] = useState({
    show: false,
    message: "",
    variant: "success",
  });

  useEffect(() => {
    getMeasures();
  }, [pagination.page]);

  useEffect(() => {
    if (isNewMeasure === true) {
      setShowEditDialog(true);
      setSelectedMeasure({});
    }
  }, [isNewMeasure]);

  const ActionsColumnFormatter = (cell, row, rowIndex) => (
    <>
      <OverlayTrigger
        overlay={
          <Tooltip id="products-edit-tooltip">
            {intl.formatMessage({ id: "GENERAL.EDIT" })}
          </Tooltip>
        }
      >
        <IconButton
          aria-label="Delete"
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
          onClick={() => openEditMeasureDialog(row)}
        >
          <EditIcon />
        </IconButton>
      </OverlayTrigger>

      <OverlayTrigger
        overlay={
          <Tooltip id="products-delete-tooltip">
            {intl.formatMessage({ id: "GENERAL.DELETE" })}
          </Tooltip>
        }
      >
        <IconButton
          aria-label="Delete"
          className="btn btn-icon btn-light btn-hover-danger btn-sm"
          onClick={() => openDeleteMeasureDialog(row)}
        >
          <DeleteIcon />
        </IconButton>
      </OverlayTrigger>
    </>
  );

  const columns = [
    {
      dataField: "description",
      text: intl.formatMessage({ id: "GENERAL.DESCRIPTION" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: (cell, row, rowIndex) => {
        return ActionsColumnFormatter(cell, row, rowIndex);
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const openDeleteMeasureDialog = (measure) => {
    setSelectedMeasure(measure);
    setShowDeleteDialog(true);
  };

  const openEditMeasureDialog = (measure) => {
    setSelectedMeasure(measure);
    setShowEditDialog(true);
  };

  const closeDeleteMeasureDialog = () => {
    setShowDeleteDialog(false);
  };

  const closeEditMeasureDialog = () => {
    setShowEditDialog(false);
    setIsNewMeasure(false);
  };

  const getMeasures = () => {
    const request = {
      sizePerPageList: pagination.sizePerPageList,
      sizePerPage: pagination.sizePerPage,
      page: pagination.page,
    };
    axiosAPI.get("/Measures", { params: request }).then(function(response) {
      setEntitie(response.data.measures);
      setPagination(response.data.pagination);
    });
  };

  const deleteMeasure = () => {
    setIsLoading(true);
    const request = { measureId: selectedMeasure.measureId };
    axiosAPI.post("/Measures/delete", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getMeasures();
      setIsLoading(false);
      setShowDeleteDialog(false);
    });
  };

  const saveMeasure= () => {
    setIsLoading(true);
    if (isNewMeasure === false) {
      editMeasure();
    } else {
      createMeasure();
    }
    getMeasures();
    setIsLoading(false);
  };

  const editMeasure = () => {
    const request = {
      measureId: selectedMeasure.measureId,
      description: selectedMeasure.description,
    };

    axiosAPI.put("/Measures", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getMeasures();
      setShowEditDialog(false);
    });
  };

  const createMeasure = () => {
    const request = {
      description: selectedMeasure.description,
    };
    axiosAPI.post("/Measures", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getMeasures();
      setShowEditDialog(false);
    });
  };

  return (
    <>
      <CustomDatatable
        entitie={entitie}
        paginationProps={pagination}
        setPaginationProps={setPagination}
        columns={columns}
      />
      <MeasureDeleteDialog
        show={showDeleteDialog}
        onHide={closeDeleteMeasureDialog}
        deleteFunction={deleteMeasure}
        isLoading={isLoading}
      />
      <MeasureEdit
        measure={selectedMeasure}
        show={showEditDialog}
        onHide={closeEditMeasureDialog}
        saveFunction={saveMeasure}
        isLoading={isLoading}
        isNewMeasure={isNewMeasure}
        setMeasure={setSelectedMeasure}
      />
      <SnackbarAlert
        variant={showSnackbar.variant}
        message={showSnackbar.message}
        show={showSnackbar.show}
      />
    </>
  );
}
