import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { MeasuresTable } from "./MeasuresTable";
import { useIntl } from "react-intl";

export function MeasuresCard() {
  const intl = useIntl();
  const [isNewMeasure, setIsNewMeasure] = useState(false);

  return (
    <Card>
      <CardHeader
        title={intl.formatMessage({ id: "MEASURES.MEASURELIST" })}
      >
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={() => setIsNewMeasure(true)}
          >
            {intl.formatMessage({ id: "MEASURES.NEW" })}
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <MeasuresTable
          isNewMeasure={isNewMeasure}
          setIsNewMeasure={setIsNewMeasure}
        />
      </CardBody>
    </Card>
  );
}
