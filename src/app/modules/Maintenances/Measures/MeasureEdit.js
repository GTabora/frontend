/* eslint-disable no-restricted-imports */
import React from "react";
import { Button, FormControl, InputGroup, Modal } from "react-bootstrap";
import { useIntl } from "react-intl";
import { ModalProgressBar } from "../../../../_metronic/_partials/controls";

export function MeasureEdit({
  measure,
  show,
  onHide,
  saveFunction,
  isLoading,
  setMeasure,
  isNewMeasure,
}) {
  const intl = useIntl();

  const updateDescription = (e) => {
    setMeasure({ ...measure, description: e.currentTarget.value });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          {isNewMeasure
            ? intl.formatMessage({ id: "GENERAL.NEW" })
            : intl.formatMessage({ id: "GENERAL.EDIT" })}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <InputGroup>
          <FormControl
            aria-label="Default"
            aria-describedby="inputGroup-sizing-default"
            placeholder={intl.formatMessage({ id: "GENERAL.DESCRIPTION" })}
            value={measure && measure.description}
            onChange={updateDescription}
          />
        </InputGroup>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            {intl.formatMessage({ id: "GENERAL.CANCEL" })}
          </button>
          <> </>
          <Button
            type="button"
            onClick={saveFunction}
            variant="outline-primary"
          >
            {isNewMeasure
              ? intl.formatMessage({ id: "GENERAL.SAVE" })
              : intl.formatMessage({ id: "GENERAL.EDIT" })}
          </Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
