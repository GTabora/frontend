import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { CategoriesTable } from "./CategoriesTable";
import { useIntl } from "react-intl";

export function CategoriesCard() {
  const intl = useIntl();
  const [isNewCategory, setIsNewCategory] = useState(false);

  return (
    <Card>
      <CardHeader
        title={intl.formatMessage({ id: "CATEGORIES.CATEGORIESLIST" })}
      >
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={() => setIsNewCategory(true)}
          >
            {intl.formatMessage({ id: "CATEGORIES.NEW" })}
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <CategoriesTable
          isNewCategory={isNewCategory}
          setIsNewCategory={setIsNewCategory}
        />
      </CardBody>
    </Card>
  );
}
