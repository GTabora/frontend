import React, { useEffect, useState } from "react";
import { useIntl } from "react-intl";
import { sortCaret } from "../../../../_metronic/_helpers";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { axiosAPI } from "../../../../API.js";
import { CategoryDeleteDialog } from "./CategoryDeleteDialog";
import { CategoryEdit } from "./CategoryEdit";
import SnackbarAlert from "../../../controls/snackbar/snackbar";
import CustomDatatable from "../../../controls/datatable/datatable";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

export function CategoriesTable({ isNewCategory, setIsNewCategory }) {
  const intl = useIntl();
  const [entitie, setEntitie] = useState([]);
  const [pagination, setPagination] = useState({
    totalSize: 10,
    sizePerPage: 3,
    page: 1,
    custom: true
    // sizePerPageList: uiHelpers.sizePerPageList
  });
  const [selectedCategory, setSelectedCategory] = useState({});
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [showEditDialog, setShowEditDialog] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showSnackbar, setShowSnackbar] = useState({
    show: false,
    message: "",
    variant: "success",
  });

  useEffect(() => {
    console.log(pagination);
    getCategories();
  }, [pagination.page]);

  useEffect(() => {
    if (isNewCategory === true) {
      setShowEditDialog(true);
      setSelectedCategory({});
    }
  }, [isNewCategory]);

  const ActionsColumnFormatter = (cell, row, rowIndex) => (
    <>
      <OverlayTrigger
        overlay={
          <Tooltip id="products-edit-tooltip">
            {intl.formatMessage({ id: "GENERAL.EDIT" })}
          </Tooltip>
        }
      >
        <IconButton
          aria-label="Delete"
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
          onClick={() => openEditCategoryDialog(row)}
        >
          <EditIcon />
        </IconButton>
      </OverlayTrigger>

      <OverlayTrigger
        overlay={
          <Tooltip id="products-delete-tooltip">
            {intl.formatMessage({ id: "GENERAL.DELETE" })}
          </Tooltip>
        }
      >
        <IconButton
          aria-label="Delete"
          className="btn btn-icon btn-light btn-hover-danger btn-sm"
          onClick={() => openDeleteCategoryDialog(row)}
        >
          <DeleteIcon />
        </IconButton>
      </OverlayTrigger>
    </>
  );

  const columns = [
    {
      dataField: "description",
      text: intl.formatMessage({ id: "GENERAL.DESCRIPTION" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: (cell, row, rowIndex) => {
        return ActionsColumnFormatter(cell, row, rowIndex);
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const openDeleteCategoryDialog = (category) => {
    setSelectedCategory(category);
    setShowDeleteDialog(true);
  };

  const openEditCategoryDialog = (category) => {
    setSelectedCategory(category);
    setShowEditDialog(true);
  };

  const closeDeletecategoryDialog = () => {
    setShowDeleteDialog(false);
  };

  const closeEditCategoryDialog = () => {
    setShowEditDialog(false);
    setIsNewCategory(false);
  };

  const getCategories = () => {
    const request = {
      sizePerPageList: pagination.sizePerPageList,
      sizePerPage: pagination.sizePerPage,
      page: pagination.page,
    };
    axiosAPI.get("/Categories", { params: request }).then(function(response) {
      setEntitie(response.data.categories);
      setPagination(response.data.pagination);
    });
  };

  const deleteCategory = () => {
    setIsLoading(true);
    const request = { categoryId: selectedCategory.categoryId };
    axiosAPI.post("/Categories/delete", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getCategories();
      setIsLoading(false);
      setShowDeleteDialog(false);
    });
  };

  const saveCategory = () => {
    setIsLoading(true);
    if (isNewCategory === false) {
      editCategory();
    } else {
      createCategory();
    }
    getCategories();
    setIsLoading(false);
  };

  const editCategory = () => {
    const request = {
      categoryId: selectedCategory.categoryId,
      description: selectedCategory.description,
    };

    axiosAPI.put("/Categories", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getCategories();
      setShowEditDialog(false);
    });
  };

  const createCategory = () => {
    const request = {
      description: selectedCategory.description,
    };
    axiosAPI.post("/Categories", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
    });
  };

  return (
    <>
      <CustomDatatable
        entitie={entitie}
        paginationProps={pagination}
        setPaginationProps={setPagination}
        columns={columns}
      />
      <CategoryDeleteDialog
        id={selectedCategory}
        show={showDeleteDialog}
        onHide={closeDeletecategoryDialog}
        deleteCategoryFunction={deleteCategory}
        isLoading={isLoading}
      />
      <CategoryEdit
        category={selectedCategory}
        show={showEditDialog}
        onHide={closeEditCategoryDialog}
        saveCategoryFunction={saveCategory}
        isLoading={isLoading}
        isNewCategory={isNewCategory}
        setCategory={setSelectedCategory}
      />
      <SnackbarAlert
        variant={showSnackbar.variant}
        message={showSnackbar.message}
        show={showSnackbar.show}
      />
    </>
  );
}
