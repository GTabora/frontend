export const defaultSorted = [{ dataField: "categoryId", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 }
];

export const initialFilter = {
  filter: {
    description: "",
  },
  sortOrder: "asc", // asc||desc
  sortField: "description",
  pageNumber: 1,
  pageSize: 10,
};
