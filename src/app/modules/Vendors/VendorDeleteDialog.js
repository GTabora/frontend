/* eslint-disable no-restricted-imports */
import React from "react";
import { Modal } from "react-bootstrap";
import { useIntl } from "react-intl";
import { ModalProgressBar } from "../../../_metronic/_partials/controls";


export function VendorDeleteDialog({ show, onHide, deleteFunction, isLoading }) {
  const intl = useIntl();

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          {intl.formatMessage({id:"GENERAL.DELETEREGISTER"})}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>{intl.formatMessage({id:"GENERAL.DELETEREGISTERQUESTION"})}</span>
        )}
        {isLoading && <span>{intl.formatMessage({id:"GENERAL.DELETEREGISTERCONFIRMATION"})}</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            {intl.formatMessage({id:"GENERAL.CANCEL"})}
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteFunction}
            className="btn btn-delete btn-elevate"
          >
             {intl.formatMessage({id:"GENERAL.DELETE"})}
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
