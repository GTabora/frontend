import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import { useIntl } from "react-intl";
import { VendorsTable } from "./VendorsTable";
import { VendorEdit } from "./VendorEdit";
import { axiosAPI } from "../../../API.js";
import SnackbarAlert from "../../controls/snackbar/snackbar";

export function VendorsCard() {
  const intl = useIntl();
  const [isNewVendor, setIsNewVendor] = useState(false);
  const [goToAnotherComponent, setGoToAnotherComponent] = useState(false);
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [selectedVendor, setSelectedVendor] = useState({
    vendorId: 0,
    name: "",
    rtn: "",
    address: "",
    email: "",
    phoneNumber: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const [entitie, setEntitie] = useState([]);
  const [pagination, setPagination] = useState({
    totalSize: 10,
    sizePerPage: 3,
    page: 1,
    custom: true,
    // sizePerPageList: uiHelpers.sizePerPageList
  });
  const [showSnackbar, setShowSnackbar] = useState({
    show: false,
    message: "",
    variant: "success",
  });

  useEffect(() => {
    getVendors();
  }, [pagination.page]);


  const saveVendor = () => {
    if (isNewVendor === false) {
      editVendor();
    } else {
      createVendor();
    }
    setIsNewVendor(false);
    setGoToAnotherComponent(false);
    getVendors();
  };

  function editVendor(){
    axiosAPI.put("/Vendors", selectedVendor).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getVendors();
    });
  };

  function createVendor() {
    axiosAPI.post("/Vendors", selectedVendor).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getVendors();
    });
  };

  const getVendors = () => {
    const request = {
      sizePerPageList: pagination.sizePerPageList,
      sizePerPage: pagination.sizePerPage,
      page: pagination.page,
    };
    axiosAPI.get("/Vendors", { params: request }).then(function(response) {
      if (response.data) {
        setEntitie(response.data.vendors);
        setPagination(response.data.pagination);
      }
    });
  };

  const goToCreateNewVendor = () => {
    setGoToAnotherComponent(true);
    setIsNewVendor(true);
  };

  const deleteVendor = () => {
    setIsLoading(true);
    const request = { vendorId: selectedVendor.vendorId };
    axiosAPI.post("/Vendors/delete", request).then(function(response) {
      if (response.data.hasValidationErrorMessage === true) {
        setShowSnackbar({
          show: true,
          variant: "warning",
          message: response.data.validationErrorMessage,
        });
        return;
      }
      setShowSnackbar({
        show: true,
        variant: "success",
        message: "Success",
      });
      getVendors();
      setIsLoading(false);
      setShowDeleteDialog(false);
    });
  };

  const openDeleteDialog = (vendor) => {
    setSelectedVendor(vendor);
    setShowDeleteDialog(true);
  };

  const openEditDialog = (vendor) => {
    setSelectedVendor(vendor);
    setGoToAnotherComponent(true);
    setIsNewVendor(false);
  };

  const closeDeleteDialog = () => {
    setShowDeleteDialog(false);
  };

  return (
    <>
      {goToAnotherComponent ? (
        <VendorEdit
          setIsNewVendor={setIsNewVendor}
          selectedVendor={selectedVendor}
          setSelectedVendor={setSelectedVendor}
          saveFunction={saveVendor}
          setGoToAnotherComponent={setGoToAnotherComponent}
        />
      ) : (
        <Card>
          <CardHeader title={intl.formatMessage({ id: "VENDORS.VENDORLIST" })}>
            <CardHeaderToolbar>
              <button
                type="button"
                className="btn btn-primary"
                onClick={goToCreateNewVendor}
              >
                {intl.formatMessage({ id: "VENDORS.NEW" })}
              </button>
            </CardHeaderToolbar>
          </CardHeader>
          <CardBody>
            <VendorsTable
              entitie={entitie}
              setPagination={setPagination}
              pagination={pagination}
              setSelectedVendor={setSelectedVendor}
              deleteVendor={deleteVendor}
              isLoading={isLoading}
              openDeleteDialog={openDeleteDialog}
              openEditDialog={openEditDialog}
              closeDeleteDialog={closeDeleteDialog}
              showDeleteDialog={showDeleteDialog}
            />
          </CardBody>
        </Card>
      )}
      {showSnackbar && showSnackbar.show ? (
        <SnackbarAlert
          variant={showSnackbar.variant}
          message={showSnackbar.message}
          show={showSnackbar.show}
        />
      ) : (
        <></>
      )}
    </>
  );
}
