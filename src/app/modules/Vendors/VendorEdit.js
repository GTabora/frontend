/* eslint-disable no-restricted-imports */
import React from "react";
import { Col, Form } from "react-bootstrap";
import { useIntl } from "react-intl";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardHeaderToolbar,
  CardHeaderTitle,
} from "../../../_metronic/_partials/controls";

export function VendorEdit({
  setGoToAnotherComponent,
  selectedVendor,
  setSelectedVendor,
  saveFunction,
}) {
  const intl = useIntl();

  return (
    <Card>
      <CardHeader>
        <CardHeaderToolbar>
          <CardHeaderTitle>
            {intl.formatMessage({ id: "VENDORS.NEW" })}
          </CardHeaderTitle>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <Form>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>
                {intl.formatMessage({ id: "VENDORS.NAME" })}
              </Form.Label>
              <Form.Control
                type="text"
                value={selectedVendor && selectedVendor.name}
                onChange={(e) =>
                  setSelectedVendor({
                    ...selectedVendor,
                    name: e.currentTarget.value,
                  })
                }
              />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label>
                {intl.formatMessage({ id: "VENDORS.RTN" })}
              </Form.Label>
              <Form.Control
                type="text"
                value={selectedVendor && selectedVendor.rtn}
                onChange={(e) =>
                  setSelectedVendor({
                    ...selectedVendor,
                    rtn: e.currentTarget.value,
                  })
                }
                required
              />
            </Form.Group>
          </Form.Row>
          <Form.Group controlId="formGridAddress1">
            <Form.Label>
              {intl.formatMessage({ id: "VENDORS.ADDRESS" })}
            </Form.Label>
            <Form.Control
              type="text"
              value={selectedVendor && selectedVendor.address}
              onChange={(e) =>
                setSelectedVendor({
                  ...selectedVendor,
                  address: e.currentTarget.value,
                })
              }
              required
            />
          </Form.Group>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>
                {intl.formatMessage({ id: "VENDORS.EMAIL" })}
              </Form.Label>
              <Form.Control
                type="email"
                value={selectedVendor && selectedVendor.email}
                onChange={(e) =>
                  setSelectedVendor({
                    ...selectedVendor,
                    email: e.currentTarget.value,
                  })
                }
                required
              />
            </Form.Group>
            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label>
                {intl.formatMessage({ id: "VENDORS.PHONENUMBER" })}
              </Form.Label>
              <Form.Control
                type="text"
                value={selectedVendor && selectedVendor.phoneNumber}
                onChange={(e) =>
                  setSelectedVendor({
                    ...selectedVendor,
                    phoneNumber: e.currentTarget.value,
                  })
                }
                required
              />
            </Form.Group>
          </Form.Row>
        </Form>
      </CardBody>
      <CardFooter>
        <button
          className="btn btn-success btn-lg btn-block"
          onClick={saveFunction}
        >
          <i className="flaticon2-check-mark"></i>
          {intl.formatMessage({ id: "GENERAL.SAVE" })}
        </button>
        <button
          className="btn btn-light-primary btn-lg btn-block"
          onClick={() => setGoToAnotherComponent(false)}
        >
          <i class="flaticon2-cancel-music"></i>
          {intl.formatMessage({ id: "GENERAL.CANCEL" })}
        </button>
      </CardFooter>
    </Card>
  );
}
