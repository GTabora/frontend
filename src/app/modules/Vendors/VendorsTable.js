import React from "react";
import { useIntl } from "react-intl";
import { sortCaret } from "../../../_metronic/_helpers";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import CustomDatatable from "../../controls/datatable/datatable";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { VendorDeleteDialog } from "./VendorDeleteDialog";
import SnackbarAlert from "../../controls/snackbar/snackbar";

export function VendorsTable({
  entitie,
  pagination,
  setPagination,
  deleteVendor,
  isLoading,
  openDeleteDialog,
  closeDeleteDialog,
  openEditDialog,
  showDeleteDialog,
}) {
  const intl = useIntl();

  const ActionsColumnFormatter = (cell, row, rowIndex) => (
    <>
      <OverlayTrigger
        overlay={
          <Tooltip id="products-edit-tooltip">
            {intl.formatMessage({ id: "GENERAL.EDIT" })}
          </Tooltip>
        }
      >
        <IconButton
          aria-label="Delete"
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
          onClick={() => openEditDialog(row)}
        >
          <EditIcon />
        </IconButton>
      </OverlayTrigger>

      <OverlayTrigger
        overlay={
          <Tooltip id="products-delete-tooltip">
            {intl.formatMessage({ id: "GENERAL.DELETE" })}
          </Tooltip>
        }
      >
        <IconButton
          aria-label="Delete"
          className="btn btn-icon btn-light btn-hover-danger btn-sm"
          onClick={() => openDeleteDialog(row)}
        >
          <DeleteIcon />
        </IconButton>
      </OverlayTrigger>
    </>
  );

  const columns = [
    {
      dataField: "name",
      text: intl.formatMessage({ id: "VENDORS.NAME" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "rtn",
      text: intl.formatMessage({ id: "VENDORS.RTN" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "address",
      text: intl.formatMessage({ id: "VENDORS.ADDRESS" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "email",
      text: intl.formatMessage({ id: "VENDORS.EMAIL" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "phoneNumber",
      text: intl.formatMessage({ id: "VENDORS.PHONENUMBER" }),
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: (cell, row, rowIndex) => {
        return ActionsColumnFormatter(cell, row, rowIndex);
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  return (
    <>
      <CustomDatatable
        entitie={entitie || []}
        paginationProps={pagination}
        setPaginationProps={setPagination}
        columns={columns}
      />
      <VendorDeleteDialog
        show={showDeleteDialog}
        onHide={closeDeleteDialog}
        deleteFunction={deleteVendor}
        isLoading={isLoading}
      />
      {/* <SnackbarAlert
        variant="warning"
        message="test"
        show={true}
      /> */}
    </>
  );
}
