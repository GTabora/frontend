import axios from "axios";

export const axiosAPI = axios.create({
    baseURL: "https://localhost:5001/api",
  });